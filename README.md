# Test assignment for Brilo

## Assignment:

Prepare frontend stack and build webpage based on the provided design.
Approach it like a big project, that could be scalable.
Design:
(https://www.figma.com/file/63rrLEozuwDRmFQxQOvfaK/%C3%9Akol-pro-
uchaze%C4%8De?node-id=1%3A2&amp;t=3JSaf9qUqvJEWwCS-1) [https://www.figma.com/file/63rrLEozuwDRmFQxQOvfaK/%C3%9Akol-pro-uchaze%C4%8De?node-id=33%3A181&mode=dev]
● Code will be in public repository
● Code in english
● Code will be properly formatted
● Readme with instructions how to run and build your project
● Use your stack to generate your website
● Use template engine of frontend framework
● Use any naming system for CSS
● Type in Readme which one you choose
● Page is responsive
● Navigation in HAM menu

- Publish your work (github pages) (doesnt have to be public)
- Expand project

## How to run:

- Clone this repository `git clone https://gitlab.com/krystof-hruza/brilo-assignment`
- Go inside the project directory `cd brilo-assignment`
- Run locally with `yarn start` or `npm run start`
- Alternatively build with `yarn build` or `npm run build` and then serve contents of _/build_ with your server solution of choice.
