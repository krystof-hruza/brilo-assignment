import { createBrowserRouter } from "react-router-dom";
import { App } from "./gui/app";
import { FuturePage } from "./gui/pages/FuturePage";
import { HomePage } from "./gui/pages/HomePage";

export const hashLinks = {
  about: "o-nas",
  services: "sluzby",
};

export const routeMap = {
  home: { path: "/", element: <HomePage /> },
  clickthrough1: { path: "/clickthrough1", element: <FuturePage /> },
  clickthrough2: { path: "/clickthrough2", element: <FuturePage /> },
  clickthrough3: { path: "/clickthrough3", element: <FuturePage /> },
};

export const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <FuturePage />,
    children: [
      { ...routeMap.home },
      { ...routeMap.clickthrough1 },
      { ...routeMap.clickthrough2 },
      { ...routeMap.clickthrough3 },
    ],
  },
]);
