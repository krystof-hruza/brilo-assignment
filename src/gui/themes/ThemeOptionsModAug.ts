import React from 'react'

declare module '@mui/material/styles' {
  interface TypographyVariants  {
    menuItem: React.CSSProperties
    bodyTextBoldLg: React.CSSProperties
    bodyTextBoldMd: React.CSSProperties
    bodyTextBoldSm: React.CSSProperties
    bodyTextMediumLg: React.CSSProperties
    bodyTextMediumMd: React.CSSProperties
    bodyTextMediumSm: React.CSSProperties
  }
  interface TypographyVariantsOptions {
    menuItem: React.CSSProperties
    bodyTextBoldLg: React.CSSProperties
    bodyTextBoldMd: React.CSSProperties
    bodyTextBoldSm: React.CSSProperties
    bodyTextMediumLg: React.CSSProperties
    bodyTextMediumMd: React.CSSProperties
    bodyTextMediumSm: React.CSSProperties
  }
}

declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    menuItem: true
    bodyTextBoldLg: true;
    bodyTextBoldMd: true;
    bodyTextBoldSm: true;
    bodyTextMediumLg: true;
    bodyTextMediumMd: true;
    bodyTextMediumSm: true;
  }
}

declare module '@mui/material/styles' {
  interface Palette {
    prim100: Palette['primary'];
    prim300: Palette['primary'];
    prim400: Palette['primary'];
    prim500: Palette['primary'];
    prim900: Palette['primary'];
    conv400: Palette['primary'];
    conv500: Palette['primary'];
    conv600: Palette['primary'];
  }

  interface PaletteOptions {
    prim100: PaletteOptions['primary'];
    prim300: PaletteOptions['primary'];
    prim400: PaletteOptions['primary'];
    prim500: PaletteOptions['primary'];
    prim900: PaletteOptions['primary'];
    conv400: PaletteOptions['primary'];
    conv500: PaletteOptions['primary'];
    conv600: PaletteOptions['primary'];
  }
}

declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides {
    prim100: true;
    prim300: true;
    prim400: true;
    prim500: true;
    prim900: true;
    conv400: true;
    conv500: true;
    conv600: true;
  }
}

declare module '@mui/material/Button' {
  interface ButtonPropsVariantOverrides {
    primary: true;
    secondary: true;
  }
}