import { alpha, createTheme } from '@mui/material';

const initialTheme = createTheme()

const paletteTheme = createTheme(
  {
    palette: {
      prim100: initialTheme.palette.augmentColor({
        color: {
          main: '#e7f0ff',
        },
        name: 'prim100',
      }),
      prim300: { main: '#7387ac' },
      prim400: { main: '#5f7aff' },
      prim500: { main: '#264afb' },
      prim900: initialTheme.palette.augmentColor({
        color: {
          main: '#00065b',
        },
        name: 'prim100',
      }),
      conv400: { main: '#06eb83' },
      conv500: { main: '#01d684' },
      conv600: { main: '#009e61' }
    }
  }
)

const typographyTheme = createTheme(paletteTheme, {
  ...paletteTheme,
  typography: {
    fontFamily: 'Montserrat Variable',
    color: paletteTheme.palette.prim900.main,
    allVariant: {
      color: paletteTheme.palette.prim900.main,
    },
    button: {
      fontSize: 14,
      fontWeight: 700,
      lineHeight: '24px',
      textTransform: 'uppercase',
      cursor: 'pointer',
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
      textDecoration: 'none'
    },
    menuItem: {
      fontSize: 14,
      fontWeight: 700,
      lineHeight: '24px',
      textTransform: 'uppercase',
      cursor: 'pointer',
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
      textDecoration: 'none',
      '&:hover': {
        borderBottom: `4px solid ${paletteTheme.palette.conv400.main}`
      },
      '&:active': {
        borderBottom: `4px solid ${paletteTheme.palette.conv400.main}`
      },
      '&.active': {
        borderBottom: `4px solid ${paletteTheme.palette.conv400.main}`
      }
    },
    h1: {
      fontSize: 48,
      lineHeight: '56px',
      fontWeight: 700,
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main
    },
    h2: {
      fontSize: 32,
      lineHeight: '40px',
      fontWeight: 700,
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
    },
    h3: {
      fontSize: 24,
      lineHeight: '32px',
      fontWeight: 700,
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
    },
    h4: {
      fontSize: 20,
      lineHeight: '32px',
      fontWeight: 700,
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
    },
    bodyTextBoldLg: {
      fontSize: 18,
      lineHeight: '24px',
      fontWeight: 700,
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
    },
    bodyTextBoldMd: {
      fontSize: 16,
      lineHeight: '24px',
      fontWeight: 700,
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
    },
    bodyTextBoldSm: {
      fontSize: 14,
      lineHeight: '24px',
      fontWeight: 700,
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
    },
    bodyTextMediumLg: {
      fontSize: 18,
      lineHeight: '24px',
      fontWeight: 500,
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
    },
    bodyTextMediumMd: {
      fontSize: 16,
      lineHeight: '24px',
      fontWeight: 500,
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
    },
    bodyTextMediumSm: {
      fontSize: 14,
      lineHeight: '24px',
      fontWeight: 500,
      fontFamily: 'Montserrat Variable',
      color: paletteTheme.palette.prim900.main,
    },
  }
});

export const defaultTheme = createTheme(typographyTheme, {
  components: {
    MuiButton: {
      variants: [
        {
          props: { variant: 'primary' },
          style: {
            font: typographyTheme.typography.button,
            backgroundColor: paletteTheme.palette.conv400.main,
            padding: '16px 40px',
            borderRadius: '8px',
            '&:hover': {
              backgroundColor: paletteTheme.palette.conv500.main,
              borderBottom: '2px solid',
              borderColor: paletteTheme.palette.conv600.main,
            }
          },
        },
        {
          props: { variant: 'secondary'},
          style: {
            font: typographyTheme.typography.button,
            padding: '16px 40px',
            borderRadius: '8px',
            color: paletteTheme.palette.conv500.main,
            border: `1px solid ${paletteTheme.palette.prim100.main}`,
            boxShadow: `0px 3px 15px 0px ${alpha(paletteTheme.palette.prim900.main, 0.1)}`,
            '&:hover': {
              color: paletteTheme.palette.prim900.main,
              backgroundColor: paletteTheme.palette.prim100.main,
              boxShadow: `0px 5px 15px 0px ${alpha(paletteTheme.palette.prim900.main, 0.3)}`
            },
            '&:active, &.active': {
              color: 'white',
              backgroundColor: paletteTheme.palette.prim500.main,
              border: '1px solid',
              borderColor: paletteTheme.palette.prim500.main,
              boxShadow: `0px 5px 15px 0px ${alpha(paletteTheme.palette.prim900.main, 0.3)}`
            }
          },
        },
      ]
    }
  }
})