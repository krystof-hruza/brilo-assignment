import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import { useEffect } from "react";
import { Outlet, useLocation } from "react-router-dom";
import { FooterBlock } from "../blocks/FooterBlock";
import { HeaderBlock } from "../blocks/HeaderBlock";
import { defaultTheme } from "../themes/defaultTheme";

export function App() {
  const { pathname, hash, key } = useLocation();

  useEffect(() => {
    if (hash === "") {
      window.scrollTo(0, 0);
    } else {
      setTimeout(() => {
        const element = document.getElementById(hash.replace("#", ""));
        element && element.scrollIntoView({ block: "start" });
      }, 0);
    }
  }, [pathname, hash, key]);

  return (
    <ThemeProvider theme={defaultTheme}>
      <CssBaseline />
      <Box sx={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}>
        <HeaderBlock />
        <Box sx={{ flexGrow: 1 }}>
          <Outlet />
        </Box>
        <FooterBlock />
      </Box>
    </ThemeProvider>
  );
}
