import { Container, Typography } from "@mui/material";

export function FuturePage() {
  return (
    <Container>
      <Typography variant="h1">Tato stránka zatím neexistuje.</Typography>;
    </Container>
  );
}
