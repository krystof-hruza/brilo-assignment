import { Box, Button, List, ListItem, Typography, alpha, useTheme } from "@mui/material";
import { hashLinks } from "../../../../routes";
import { DuotileBlock } from "../../../blocks/DuotileBlock";

export function DuoblocksPart() {
  const theme = useTheme();
  return (
    <Box id={hashLinks.about} sx={{ "&::before": { height: "82px", content: '""', display: "block" } }}>
      <DuotileBlock side="left" image="/photos/dd55d329b6130c5b79b393c3bbfe1b96.jpeg" noTopPadding centerY>
        <>
          <Typography variant="h1">Hlavní nadpis webu</Typography>
          <Typography variant="bodyTextMediumMd">
            V k žert planetu rysů obdivují stravování starověkého zebřičky u tahů zimující. Akcí a důkaz pomoci narozen
            muzea signálem.
          </Typography>
          <Box gap="32px" display="flex" marginTop="64px">
            <Button variant="secondary">Více informací</Button>
            <Button variant="primary">Poptat nabídku</Button>
          </Box>
        </>
      </DuotileBlock>
      <DuotileBlock
        side="right"
        image="/photos/56e99ddb5766c59aefa54fcaef6d0ee3.jpeg"
        bgColor={alpha(theme.palette.prim100.main, 0.5)}
      >
        <>
          <Typography variant="h2">Nechte své peníze růst</Typography>
          <Typography variant="bodyTextMediumMd" sx={{ marginBottom: "64px", marginTop: "16px" }}>
            V k žert planetu rysů obdivují stravování starověkého zebřičky u tahů zimující. Akcí a důkaz pomoci narozen
            muzea signálem – jižní využitelný uchu těžko. Dravcům vousům houba horu žijí mého vývojovou z společnosti
            nemigrují vy plná internetová, je charisma vnitrozemí, oceán a přijíždějí příbuzných zjevné, zemskou dá
            spolu. Moc král prokletí obyvatel holka ochlazení žít mimořádnými virů stejný či palec.
            <List sx={{ marginTop: "1em" }}>
              <ListItem disableGutters disablePadding>
                Musel za až angličtinu látky nohy deprimovaná polokouli i roku map.
              </ListItem>
              <ListItem disableGutters disablePadding>
                Těžko popsal, ještě zúročovat však, by čase musel mi nuly, ta naší.
              </ListItem>
              <ListItem disableGutters disablePadding>
                Musel za až angličtinu látky nohy deprimovaná polokouli i roku map.
              </ListItem>
              <ListItem disableGutters disablePadding>
                Těžko popsal, ještě zúročovat však, by čase musel mi nuly, ta naší.
              </ListItem>
            </List>
          </Typography>
          <Box sx={{ display: "flex" }}>
            <Button variant="primary">Zjistit více</Button>
          </Box>
        </>
      </DuotileBlock>
      <DuotileBlock side="left" image="/photos/56e99ddb5766c59aefa54fcaef6d0ee3.jpeg">
        <>
          <Typography variant="h2">Nechte své peníze růst</Typography>
          <Typography variant="bodyTextMediumMd" sx={{ marginBottom: "64px", marginTop: "16px" }}>
            V k žert planetu rysů obdivují stravování starověkého zebřičky u tahů zimující. Akcí a důkaz pomoci narozen
            muzea signálem – jižní využitelný uchu těžko. Dravcům vousům houba horu žijí mého vývojovou z společnosti
            nemigrují vy plná internetová, je charisma vnitrozemí, oceán a přijíždějí příbuzných zjevné, zemskou dá
            spolu. Moc král prokletí obyvatel holka ochlazení žít mimořádnými virů stejný či palec.
          </Typography>
          <Box gap="32px" display="flex">
            <Button variant="primary">Zjistit více</Button>
          </Box>
        </>
      </DuotileBlock>
    </Box>
  );
}
