import { Box, Button, Container, MenuItem, Select, SelectChangeEvent, Typography } from "@mui/material";
import { useState } from "react";
import { hashLinks } from "../../../../routes";

export function TilesPart() {
  const [filter, setFilter] = useState("all");
  const filterList = [
    { label: "Všechno", key: "all" },
    { label: "Papírové tašky", key: "paper" },
    { label: "Látkové tašky", key: "cloth" },
    { label: "Igelitové tašky", key: "plastic" },
  ];

  const tileList = [
    {
      category: "cloth",
      image: "/photos/6d977d64fe57c7a806e2e2e4e3cbdc08.jpeg",
      label: "Látkové tašky s potiskem",
      description: "Dlouhá význam s sionismu ty, jí antény i sezona, bílá malých izolovány uvést masové vodě",
      action: () => {},
    },
    {
      category: "cloth",
      image: "/photos/a7f4e2f9f951c4e54bd63e6babd57800.jpeg",
      label: "Látkové tašky bez potisku",
      description: "Dlouhá význam s sionismu ty, jí antény i sezona",
      action: () => {},
    },
    {
      category: "paper",
      image: "/photos/bdf2303a09d9a26f9c5ac824ac3f026d.jpeg",
      label: "Bílé papírové tašky",
      description: "Dlouhá význam s sionismu ty, jí antény i sezona, bílá malých izolovány uvést masové vodě",
      action: () => {},
    },
    {
      category: "paper",
      image: "/photos/b407e6f8dc6ea9a9aa280411064f4633.jpeg",
      label: "Barevné papírové tašky na dárky",
      description: "Dlouhá význam s sionismu ty, jí antény i sezona, bílá malých izolovány uvést masové vodě",
      action: () => {},
    },
    {
      category: "paper",
      image: "/photos/bdf2303a09d9a26f9c5ac824ac3f026d.jpeg",
      label: "Papírové tašky z recyklovaného materiálu",
      description: "Dlouhá význam s sionismu ty, jí antény i sezona",
      action: () => {},
    },
    {
      category: "plastic",
      image: "/photos/6d977d64fe57c7a806e2e2e4e3cbdc08.jpeg",
      label: "Igelitové sáčky s logem",
      description: "Dlouhá význam s sionismu ty, jí antény i sezona, bílá malých izolovány uvést masové vodě",
      action: () => {},
    },
    {
      category: "plastic",
      image: "/photos/a7f4e2f9f951c4e54bd63e6babd57800.jpeg",
      label: "Velké igelitové tašky s potiskem",
      description: "Dlouhá význam s sionismu ty, jí antény i sezona, bílá malých izolovány uvést masové vodě",
      action: () => {},
    },
  ];

  return (
    <Container
      maxWidth="xl"
      id={hashLinks.services}
      sx={{ "&::before": { height: "82px", content: '""', display: "block" } }}
    >
      <Box display="flex" flexDirection="column" justifyContent="center" gap="24px" marginBottom="64px">
        <Typography variant="h2" alignSelf="center">
          Vyberte jednu z nabízených služeb
        </Typography>
        <Typography variant="bodyTextMediumMd">
          Hlasu zkrátka nevratné duší indičtí půlkilometrová začali nutné už od středisko. Společných snažila líně
          budoucnost začne vloženy stal objevováním, umělé cílem starým dne větvičky názvy moři zabíjí.
        </Typography>
      </Box>
      <Box sx={{ display: { xs: "none", md: "flex" }, gap: "32px", justifyContent: "center", marginBottom: "64px" }}>
        {filterList.map((f) => (
          <Button
            variant="secondary"
            key={f.key}
            onClick={() => setFilter(f.key)}
            className={f.key === filter ? "active" : undefined}
          >
            {f.label}
          </Button>
        ))}
      </Box>
      <Box sx={{ display: { xs: "flex", md: "none", marginBottom: "64px" } }}>
        <Select value={filter} onChange={(e: SelectChangeEvent) => setFilter(e.target.value)}>
          {filterList.map((f) => (
            <MenuItem value={f.key} key={f.key}>
              <Typography variant="button">{f.label}</Typography>
            </MenuItem>
          ))}
        </Select>
      </Box>
      <Box
        sx={{ display: "flex", flexWrap: "wrap", gap: "32px", justifyContent: { xs: "space-around", md: "initial" } }}
      >
        {tileList
          .filter((t) => filter === "all" || t.category === filter)
          .map((t, i) => {
            return (
              <Box
                key={i}
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  width: "328px",
                  gap: "16px",
                  paddingBottom: "16px",
                }}
              >
                <Box
                  sx={{
                    width: "328px",
                    height: "328px",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    overflow: "hidden",
                  }}
                >
                  <img src={t.image} style={{ width: "auto", height: "328px" }} alt="Ilustrační obrázek" />
                </Box>
                <Box sx={{ display: "flex", flexDirection: "column", alignItems: "center", flexGrow: 1, gap: "16px" }}>
                  <Typography variant="h4">{t.label}</Typography>
                  <Typography variant="bodyTextMediumMd">{t.description}</Typography>
                </Box>
                <Button variant="primary" onClick={t.action}>
                  Zjistit více
                </Button>
              </Box>
            );
          })}
      </Box>
    </Container>
  );
}
