import { Box } from "@mui/material";
import { DuoblocksPart } from "./parts/DuoblocksPart";
import { TilesPart } from "./parts/TilesPart";

export function HomePage() {
  return (
    <Box marginBottom="112px">
      <DuoblocksPart />
      <TilesPart />
    </Box>
  );
}
