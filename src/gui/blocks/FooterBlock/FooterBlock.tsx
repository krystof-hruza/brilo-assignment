import { Box, Container, Toolbar, Typography, useTheme } from "@mui/material";
import { Link } from "react-router-dom";
import { routeMap } from "../../../routes";

export function FooterBlock() {
  const theme = useTheme();
  const clickThroughs = [
    { label: "První proklik", link: routeMap.clickthrough1.path },
    { label: "Druhý proklik", link: routeMap.clickthrough2.path },
    { label: "Třetí proklik", link: routeMap.clickthrough3.path },
  ];
  return (
    <Container maxWidth="xl" disableGutters>
      <Box sx={{ borderTop: `8px solid ${theme.palette.prim100.main}`, padding: "64px" }}>
        <Toolbar
          sx={{
            display: "flex",
            justifyContent: "space-between",
            flexDirection: { xs: "column", md: "row" },
            gap: { xs: "32px", md: "initial" },
          }}
        >
          <Link to={routeMap.home.path} style={{ paddingRight: "32px" }}>
            <img src="/logo-brilo.svg" alt="Logo Brilo" />
          </Link>
          <Box display="flex" gap="40px">
            {clickThroughs.map((ct, i) => (
              <Typography variant="bodyTextBoldMd" color={theme.palette.conv500.main} key={i}>
                <Link to={ct.link} style={{ color: "inherit" }}>
                  {ct.label}
                </Link>
              </Typography>
            ))}
          </Box>
          <Typography variant="bodyTextMediumMd">© 2022 Thalion All rights reserved.</Typography>
        </Toolbar>
      </Box>
    </Container>
  );
}
