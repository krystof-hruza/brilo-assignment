import { Box, Container, Grid } from "@mui/material";

export interface DuotileBlockProps {
  children: JSX.Element;
  image: string;
  side: "left" | "right";
  bgColor?: string;
  noTopPadding?: boolean;
  centerY?: boolean;
}

export function DuotileBlock({ children, image, side, bgColor, noTopPadding, centerY }: DuotileBlockProps) {
  const gridItem = (
    <Grid
      item
      md={6}
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: centerY ? "center" : "flex-start",
        padding: { xs: "16px", md: 0 },
        maxWidth: { xs: "100%", md: "calc(50% - 20px)" },
      }}
    >
      {children}
    </Grid>
  );
  return (
    <Box sx={{ backgroundColor: bgColor ?? "initial" }}>
      <Container maxWidth="xl" disableGutters>
        <Box
          sx={{
            display: "flex",
            flexDirection: { xs: "column", md: "row" },
            alignItems: "stretch",
            justifyContent: "space-between",
            paddingBottom: "112px",
            paddingTop: noTopPadding ? undefined : "112px",
            paddingX: { xs: "16px", xl: 0 },
            gap: "40px",
          }}
        >
          {side === "left" ? gridItem : null}
          <Box sx={{ padding: 0, maxWidth: { xs: "100%", md: "calc(50% - 20px)" } }}>
            <img src={image} style={{ width: "100%", height: "auto", maxHeight: "100%" }} alt="Ilustrační obrázek" />
          </Box>
          {side === "right" ? gridItem : null}
        </Box>
      </Container>
    </Box>
  );
}
