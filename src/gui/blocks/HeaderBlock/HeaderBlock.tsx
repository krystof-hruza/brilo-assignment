import { AppBar, Container, Toolbar } from "@mui/material";
import { useMemo } from "react";
import { Link } from "react-router-dom";
import { hashLinks, routeMap } from "../../../routes";
import { AppNavLinkType } from "../../../types/LinkList";
import { NavMenuDesktop } from "./parts/NavMenuDesktop";
import { NavMenuMobile } from "./parts/NavMenuMobile";

export function HeaderBlock() {
  const links: AppNavLinkType[] = useMemo(
    () => [
      { id: "o-nas", label: "O nás", link: `/#${hashLinks.about}` },
      {
        id: "sluzby",
        label: "Služby",
        children: [
          { label: "Submenu", link: `/#${hashLinks.services}` },
          { label: "Submenu", link: `/#${hashLinks.services}` },
          { label: "Submenu", link: `/#${hashLinks.services}` },
        ],
      },
      { id: "aktuality", label: "Aktuality" },
      { id: "novinky", label: "Novinky" },
      { id: "kontakty", label: "Kontakty" },
    ],
    []
  );

  return (
    <Container maxWidth="xl" sx={{ position: "sticky", top: 0, zIndex: 999 }} disableGutters>
      <AppBar sx={{ backgroundColor: "white", boxShadow: "none", padding: "13px 0" }} position="static">
        <Toolbar
          sx={{ display: "flex", justifyContent: "space-between", paddingX: { xs: "16px", xl: 0 } }}
          disableGutters
        >
          <Link to={routeMap.home.path}>
            <img src="/logo-brilo.svg" alt="Logo Brilo" />
          </Link>
          <NavMenuMobile items={links} />
          <NavMenuDesktop items={links} />
        </Toolbar>
      </AppBar>
    </Container>
  );
}
