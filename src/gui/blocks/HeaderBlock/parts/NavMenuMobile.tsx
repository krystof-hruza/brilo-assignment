import MenuIcon from "@mui/icons-material/Menu";
import { Box, IconButton, Menu, useTheme } from "@mui/material";
import { useState } from "react";
import { AppNavLinkType } from "../../../../types/LinkList";
import { NavMenuItem } from "./NavMenuItem";

export function NavMenuMobile({ items }: { items: AppNavLinkType[] }) {
  const theme = useTheme();
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <Box
      sx={{
        display: {
          xs: "flex",
          md: "none",
        },
      }}
    >
      <IconButton
        size="large"
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleOpenNavMenu}
        sx={{ color: theme.palette.prim900.main }}
      >
        <MenuIcon />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        keepMounted
        disableScrollLock
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}
        MenuListProps={{ sx: { flexDirection: "column", display: "flex" } }}
        sx={{
          display: { xs: "flex", md: "none" },
          flexDirection: "column",
        }}
      >
        {items.map((item, i) => (
          <NavMenuItem item={item} key={i} parentSetAnchorEl={setAnchorEl} />
        ))}
      </Menu>
    </Box>
  );
}
