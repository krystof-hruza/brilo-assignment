import { Menu, MenuItem, Typography } from "@mui/material";
import { useCallback, useRef, useState } from "react";
import { Link } from "react-router-dom";
import { AppNavLinkType } from "../../../../types/LinkList";

export function NavMenuItem({
  item,
  parentSetAnchorEl,
}: {
  item: AppNavLinkType;
  parentSetAnchorEl: (value: HTMLElement | null) => void;
}) {
  const ref = useRef<HTMLElement>(null);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const handleClick = useCallback(
    (e: React.MouseEvent<HTMLElement>, item: AppNavLinkType) => {
      item.children && setAnchorEl(e.currentTarget);
    },
    [setAnchorEl]
  );
  const Label = useCallback(
    ({ item, isLink }: { item: AppNavLinkType; isLink?: boolean }) => (
      <Typography
        ref={ref}
        variant="menuItem"
        onClick={(e) => handleClick(e, item)}
        sx={{ padding: "16px 32px", display: "flex", alignItems: "center" }}
      >
        {isLink && item.link ? (
          <Link to={item.link} style={{ color: "inherit", textDecoration: "none" }}>
            {item.label}
          </Link>
        ) : (
          item.label
        )}
        {item.children && (
          <img src="/icons/arrow_prototyp R.svg" alt="Šipka" style={{ rotate: "90deg", marginLeft: "8px" }} />
        )}
      </Typography>
    ),
    [ref]
  );

  return item.children ? (
    <>
      {<Label item={item} />}
      <Menu
        open={!!anchorEl}
        anchorEl={anchorEl}
        onClose={() => {
          setAnchorEl(null);
        }}
      >
        {item.children.map((s, i) => (
          <MenuItem
            key={i}
            onClick={() => {
              setAnchorEl(null);
              parentSetAnchorEl(null);
            }}
          >
            <Link
              to={s.link}
              style={{
                color: "unset",
                textDecoration: "none",
                display: "flex",
                justifyContent: "space-between",
                padding: "16px",
                width: "240px",
              }}
            >
              <Typography variant="button">{s.label}</Typography>
              <img src="/icons/arrow_prototyp R.svg" alt="šipka" />
            </Link>
          </MenuItem>
        ))}
      </Menu>
    </>
  ) : (
    <Label item={item} isLink />
  );
}
