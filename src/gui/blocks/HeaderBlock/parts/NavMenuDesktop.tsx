import { Box, Menu, MenuItem, Typography } from "@mui/material";
import React, { useCallback, useRef, useState } from "react";
import { Link } from "react-router-dom";
import { AppNavLinkType } from "../../../../types/LinkList";

export function NavMenuDesktop({ items }: { items: AppNavLinkType[] }) {
  return (
    <Box
      sx={{
        display: {
          xs: "none",
          md: "flex",
        },
      }}
    >
      {items.map((item: AppNavLinkType, i) => (
        <NavMenuItem key={i} item={item} />
      ))}
    </Box>
  );
}

const NavMenuItem = ({ item }: { item: AppNavLinkType }) => {
  const ref = useRef<HTMLElement>(null);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const handleClick = useCallback(
    (e: React.MouseEvent<HTMLElement>, item: AppNavLinkType) => {
      item.children && setAnchorEl(e.currentTarget);
    },
    [setAnchorEl]
  );
  const Label = useCallback(
    ({ item, isLink }: { item: AppNavLinkType; isLink?: boolean }) => (
      <Typography ref={ref} variant="menuItem" onClick={(e) => handleClick(e, item)} sx={{ paddingX: "8px" }}>
        {isLink && item.link ? (
          <Link to={item.link} style={{ color: "inherit", textDecoration: "none" }}>
            {item.label}
          </Link>
        ) : (
          item.label
        )}
      </Typography>
    ),
    [ref]
  );

  return item.children ? (
    <>
      {<Label item={item} />}
      <Menu open={!!anchorEl} anchorEl={anchorEl} onClose={() => setAnchorEl(null)}>
        {item.children.map((s, i) => (
          <MenuItem key={i} onClick={() => setAnchorEl(null)}>
            <Link
              to={s.link}
              style={{
                color: "unset",
                textDecoration: "none",
                display: "flex",
                justifyContent: "space-between",
                padding: "16px",
                width: "240px",
              }}
            >
              <Typography variant="button">{s.label}</Typography>
              <img src="/icons/arrow_prototyp R.svg" alt="šipka" />
            </Link>
          </MenuItem>
        ))}
      </Menu>
    </>
  ) : (
    <Label item={item} isLink />
  );
};
