export interface AppNavLinkType {
  /**
   * Must be unique
   */
  id: string
  label: string
  link?: string
  children?: Required<Omit<AppNavLinkType, 'children' | 'id'>>[]
}